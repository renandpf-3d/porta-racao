
int pinOutMotorA = 10;
int pinOutMotorB = 11;
int pinInPortionSensor = 8;
int pinInPortionEndSensor = 9;

const long secondsToStartWork = 10;
long timeElapsed = 0;

const int WAITING_CASE = 0;
const int SERVE_PORTION_CASE = 1;
const int END_WORK_CASE = 2;
int currentCase = WAITING_CASE;

boolean ledOn = true;

const int maxSpeedMotor = 30;
int speedMotor = 0;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pinOutMotorA, OUTPUT);
  pinMode(pinOutMotorB, OUTPUT);
  pinMode(pinInPortionSensor, INPUT);
  pinMode(pinInPortionEndSensor, INPUT);

  //Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:

  changeCase();

  execute();

  sleepAndIncrementTime();
  
  onOffLed();
}

void execute(){
  switch (currentCase) {
    case WAITING_CASE:
      digitalWrite(pinOutMotorA, LOW);
      digitalWrite(pinOutMotorB, LOW);
      break;

    case END_WORK_CASE:
      digitalWrite(pinOutMotorA, LOW);
      break;
      
    case SERVE_PORTION_CASE:
      analogWrite(pinOutMotorA, getAndUpdateSpeedMotor());
      digitalWrite(pinOutMotorB, LOW);
      break;

  }
}

void changeCase(){

  //Serial.println(getTimeElapsedInSeconds());
  
  if(digitalRead(pinInPortionEndSensor) == HIGH){
    currentCase = END_WORK_CASE;
    
  } else if( currentCase == WAITING_CASE && isTimeToWork()){
    currentCase = SERVE_PORTION_CASE;
    
  } else if( currentCase == SERVE_PORTION_CASE && digitalRead(pinInPortionSensor) == HIGH) {
    currentCase = WAITING_CASE;
    timeElapsed = 0;
    speedMotor = 0;
  }
}

void sleepAndIncrementTime(){
  const long timeDelay = 100;
  delay(timeDelay);
  timeElapsed = timeElapsed + timeDelay;
}

boolean isTimeToWork(){
  return getTimeElapsedInSeconds() >= secondsToStartWork;
}

long getTimeElapsedInSeconds(){
  return timeElapsed/1000;
}

void onOffLed(){
  if(ledOn){
    digitalWrite(LED_BUILTIN, HIGH);
    ledOn = false;
  } else {
    digitalWrite(LED_BUILTIN, LOW);
    ledOn = true;
  }
}

int getAndUpdateSpeedMotor(){
  if(speedMotor <= maxSpeedMotor){
    return speedMotor++;
  }
  speedMotor = maxSpeedMotor;
  return speedMotor;
}
